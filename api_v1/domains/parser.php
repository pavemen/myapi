<?php
/**
 * Mimics MyBB 1.6 parser for post related requests to API v1
 *
 * PHP version 5.3
 *
 * LICENSE: This software is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0
 * International License.
 *
 * @author     CommunityPlugins http://communityplugins.com
 * @copyright  2013-2014 Community Plugins
 * @license    http://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version    1.0
 * @link       https://bitbucket.org/pavemen/myapi
 */
 
if(!defined('IN_API'))
{
	die('Direct initialization is not allowed.');
}

/**
 * Parser class
 * @api
 * @version 1.0
 */
class parser extends myAPI
{
	/**
	 * Class constructor
	 *
	 * @param            $request
	 * @param \MyBB      $mybbIn  Our MyBB object.
	 * @param            $dbIn
	 * @param \datacache $cacheIn Our cache oject.
	 */
	public function parser($request, MyBB $mybbIn, $dbIn, datacache $cacheIn)
	{
		parent::__construct($request, $mybbIn, $dbIn, $cacheIn);
	}

	public function setParser($args)
	{
		require_once MYBB_ROOT."inc/class_parser.php";
		$parser = new postParser;
		
		//Default options for MyAPI - normally we should receive an options array but otherwise use them...
		$options = array(
			"allow_html"		=> false,
			"allow_mycode"		=> true,
			"allow_smilies"		=> true,
			"allow_imgcode"		=> true,
			"allow_videocode"	=> true,
			"filter_badwords"	=> true,
			"me_username"		=> ""
		);
		
		$options = array_merge($options, $this->data['options']);
		$message = $parser->parse_message($this->data['message'], $options);
		
		return array("message"=>$message);
	}
}