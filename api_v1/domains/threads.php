<?php
/**
 * Handles thread related requests to API v1
 *
 * PHP version 5.3
 *
 * LICENSE: This software is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0
 * International License.
 *
 * @author     CommunityPlugins http://communityplugins.com
 * @copyright  2013-2014 Community Plugins
 * @license    http://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version    1.0
 * @link       https://bitbucket.org/pavemen/myapi
 */
 
if(!defined('IN_API'))
{
	die('Direct initialization is not allowed.');
}

/**
 * Threads class
 * @api
 * @version 1.0
 */
class threads extends myAPI
{

	private $noAuthReqd = "tid,fid,subject,prefix,icon,poll,uid,username,dateline,firstpost,lastpost,lastposter,lastposteruid,views,replies,closed,sticky,numratings,totalratings,attachmentcount";

	/**
	 * Class constructor
	 *
	 * @param            $request
	 * @param \MyBB      $mybbIn  Our MyBB object.
	 * @param            $dbIn
	 * @param \datacache $cacheIn Our cache object.
	 */
	public function threads($request, MyBB $mybbIn, $dbIn, datacache $cacheIn)
	{
		parent::__construct($request, $mybbIn, $dbIn, $cacheIn);
	}

	public function getThreads($id = 0)
	{
		if($id == 0)
		{
			$where = '1=1';
		}
		else
		{
			$where = 'tid='.(int)$id;
		}

		if($this->isauth == 1)
		{
			$fields = '*';
		}
		else
		{
			$fields = $this->noAuthReqd.",visible";
		}

		$query  = $this->db->simple_select('threads', $fields, $where);
		$result = array();
		while($row = $this->db->fetch_array($query))
		{
			//Check if unapproved or now
			if(is_moderator($row['fid']))
			{
				if($row['visible'] > 1)
				{
					continue;
				}
			}
			else
			{
				if($row['visible'] == 0)
				{
					continue;
				}
			}

			$fpermissions = forum_permissions($row['fid']);
			//Can the user view the thread?
			if($fpermissions['canview'] != 1 || $fpermissions['canviewthreads'] != 1)
			{
				continue;
			}
			if(isset($forumpermissions['canonlyviewownthreads']) && $forumpermissions['canonlyviewownthreads'] == 1 && $row['uid'] != $this->mybb->user['uid'])
			{
				continue;
			}

			$forum = get_forum($row['fid']);
			if(!$forum || $forum['type'] != "f")
			{
				continue;
			}

			if(!$this->check_forum_password($row['fid']))
			{
				continue;
			}

			if($this->isauth != 1)
			{
				unset($row['visible']);
			}
			$result[$row['tid']] = $row;
		}

		if(count($result) == 0)
		{
			$this->setStatus(404);
		}

		if(count($result) > $this->per_page)
		{
			$result = array_slice($result, $this->start, $this->per_page, true);
		}

		return $result;
	}

	public function setThreads($id = 0)
	{
		require_once(MYBB_ROOT.'inc/datahandlers/post.php');

		$thread = $this->data;
		unset($thread['token']);

		if(count($thread) == 0)
		{
			$this->setStatus(400);

			return 0;
		}

		if($id > 0)
		{
			if(!isset($thread['tid']))
			{
				$thread['tid'] = $tid;
			}

			$old_thread = get_thread($thread['tid']);
			$forum      = get_forum($old_thread['fid']);
			if(!$forum || $forum['type'] != "f")
			{
				$this->setStatus(400);

				return 0;
			}
			if($forum['open'] == 0 || $this->mybb->user['suspendposting'] == 1)
			{
				$this->setStatus(401);

				return 0;
			}
			if(!is_moderator($old_thread['fid'], "caneditposts"))
			{
				$fpermission = forum_permissions($old_thread['fid']);
				$post        = get_post($old_thread['firstpost']);

				if($old_thread['closed'] == 1 || $fpermission['caneditposts'] == 0 || $this->mybb->user['uid'] != $post['uid'] || $post['visible'] == 0)
				{
					$this->setStatus(401);

					return 0;
				}

				if($this->mybb->settings['edittimelimit'] != 0 && $post['dateline'] < (TIME_NOW - ($this->mybb->settings['edittimelimit'] * 60)))
				{
					$this->setStatus(400);

					return 0;
				}
			}

			if(!isset($thread['pid']))
			{
				$thread['pid'] = $old_thread['firstpost'];
			}
			$posthandler         = new PostDataHandler('update');
			$posthandler->action = "post";
			$posthandler->set_data($thread);
			if(!$posthandler->validate_post())
			{
				$this->setStatus(400);

				return array('message' => $posthandler->errors);
			}
			$posthandler->update_post();

			return $this->getThreads($thread['tid']);
		}
		else
		{
			unset($thread['tid']);
			$posthandler         = new PostDataHandler('insert');
			$posthandler->action = "thread";

			if(!isset($thread['uid']) && !isset($thread['username']))
			{
				$thread['uid']      = $this->mybb->user['uid'];
				$thread['username'] = $this->mybb->user['username'];
			}

			$forum = get_forum($thread['fid']);
			if(!$forum)
			{
				$this->setStatus(400);

				return 0;
			}

			if($forum['open'] == 0 || $forum['type'] != "f" || $forum['linkto'] != "")
			{
				$this->setStatus(400);

				return 0;
			}

			$fpermission = forum_permissions($thread['fid']);
			if($fpermission['canview'] == 0 || $fpermission['canpostthreads'] == 0 || $this->mybb->user['suspendposting'] == 1)
			{
				$this->setStatus(401);

				return 0;
			}

			if(!$this->check_forum_password($thread['fid']))
			{
				$this->setStatus(401);

				return 0;
			}

			$posthandler->set_data($thread);
			if(!$posthandler->validate_thread())
			{
				$this->setStatus(400);

				return array('message' => $posthandler->errors);
			}
			$posthandler->insert_thread();

			return $this->getThreads($posthandler->tid);
		}
	}

	public function setDelete($id = 0)
	{
		if($id <= 0)
		{
			$this->setStatus(400);

			return 0;
		}

		$old_thread = get_thread($id);
		$forum      = get_forum($old_thread['fid']);
		if(!$forum || $forum['type'] != "f")
		{
			$this->setStatus(400);

			return 0;
		}
		if($forum['open'] == 0 || $this->mybb->user['suspendposting'] == 1)
		{
			$this->setStatus(401);

			return 0;
		}
		if(!is_moderator($old_thread['fid'], "candeleteposts"))
		{
			$fpermission = forum_permissions($old_thread['fid']);
			$post        = get_post($old_thread['firstpost']);
			if($old_thread['closed'] == 1 || $fpermission['candeleteposts'] == 0 || $this->mybb->user['uid'] != $post['uid'] || $post['visible'] == 0)
			{
				$this->setStatus(401);

				return 0;
			}
		}

		delete_thread($id);
		mark_reports($id, "thread");
	}

	public function getLatest($count = 10)
	{
		if($count == 0)
		{
			$count = 10;
		}
		if($this->isauth == 1)
		{
			$fields = '*';
		}
		else
		{
			$fields = $this->noAuthReqd.",visible";
		}

		$query  = $this->db->simple_select('threads', $fields, "", array("order_by" => "dateline", "order_dir" => "desc"));
		$result = array();
		while($row = $this->db->fetch_array($query))
		{
			//Check if unapproved or now
			if(is_moderator($row['fid']))
			{
				if($row['visible'] > 1)
				{
					continue;
				}
			}
			else
			{
				if($row['visible'] == 0)
				{
					continue;
				}
			}

			$fpermissions = forum_permissions($row['fid']);
			//Can the user view the thread?
			if($fpermissions['canview'] != 1 || $fpermissions['canviewthreads'] != 1)
			{
				continue;
			}
			if(isset($forumpermissions['canonlyviewownthreads']) && $forumpermissions['canonlyviewownthreads'] == 1 && $row['uid'] != $this->mybb->user['uid'])
			{
				continue;
			}

			$forum = get_forum($row['fid']);
			if(!$forum || $forum['type'] != "f")
			{
				continue;
			}

			if(!$this->check_forum_password($row['fid']))
			{
				continue;
			}

			if($this->isauth != 1)
			{
				unset($row['visible']);
			}
			$result[$row['tid']] = $row;
		}

		if(count($result) == 0)
		{
			$this->setStatus(404);
		}
		
		if(count($result) > $count)
		{
			$result = array_slice($result, ($this->page - 1) * $count, $count, true);
		}

		return $result;
	}

	function getPosts($id = 0)
	{
		if($id <= 0)
		{
			$this->setStatus(400);

			return 0;
		}

		$auth         = $this->isauth;
		$this->isauth = 1;
		$thread       = $this->getThreads($id);
		$this->isauth = $auth;

		if($this->getStatus() != 200)
		{
			return 0;
		}

		$thread = @reset($thread);
		if(!$thread['tid'] || $thread['tid'] != $id)
		{
			$this->setStatus(400);

			return 0;
		}

		$visible = "AND p.visible='1'";
		if(is_moderator($thread['fid']))
		{
			$visible = "AND (p.visible='0' OR p.visible='1')";
		}

		$pids  = $comma = "";
		$query = $this->db->simple_select("posts p", "p.pid", "p.tid='{$id}' {$visible}", array("order_by" => "p.dateline", "limit_start" => 0, "limit" => 100));
		while($getid = $this->db->fetch_array($query))
		{
			$pids .= "{$comma}'{$getid['pid']}'";
			$comma = ",";
		}
		if($pids != "")
		{
			$pids = "pid IN({$pids})";
		}
		else
		{
			$this->setStatus(400);

			return 0;
		}

		$query = $this->db->query("
			SELECT u.*, u.username AS userusername, p.*, f.*, eu.username AS editusername
			FROM ".TABLE_PREFIX."posts p
			LEFT JOIN ".TABLE_PREFIX."users u ON (u.uid=p.uid)
			LEFT JOIN ".TABLE_PREFIX."userfields f ON (f.ufid=u.uid)
			LEFT JOIN ".TABLE_PREFIX."users eu ON (eu.uid=p.edituid)
			WHERE {$pids}
			ORDER BY p.dateline
		");

		$groupscache = $this->cache->read("usergroups");
		if(!is_array($groupscache))
		{
			$this->cache->update_usergroups();
			$groupscache = $this->cache->read("usergroups");
		}

		while($post = $this->db->fetch_array($query))
		{
			if($thread['firstpost'] == $post['pid'] && $thread['visible'] == 0)
			{
				$post['visible'] = 0;
			}

			if(!$post['username'])
			{
				$post['username'] = "Guest";
			}
			if($post['userusername'])
			{
				if(!$post['displaygroup'])
				{
					$post['diplaygroup'] = $post['usergroup'];
				}
				$usergroup = $groupscache[$post['displaygroup']];
			}
			else
			{
				$usergroup = $groupscache[1];
			}

			if(!is_array($titlescache))
			{
				$cached_titles = $this->cache->read("usertitles");
				if(!empty($cached_titles))
				{
					foreach($cached_titles as $usertitle)
					{
						$titlescache[$usertitle['posts']] = $usertitle;
					}
				}

				if(is_array($titlescache))
				{
					krsort($titlescache);
				}
				unset($usertitle, $cached_titles);
			}

			if($post['userusername'])
			{
				// This post was made by a registered user
				$post['username']           = $post['userusername'];
				$post['profilelink']        = get_profile_link($post['uid']);
				$post['username_formatted'] = format_name($post['username'], $post['usergroup'], $post['displaygroup']);

				if(trim($post['usertitle']) != "")
				{
					$hascustomtitle = 1;
				}

				if($usergroup['usertitle'] != "" && !$hascustomtitle)
				{
					$post['usertitle'] = $usergroup['usertitle'];
				}
				elseif(is_array($titlescache) && !$usergroup['usertitle'])
				{
					reset($titlescache);
					foreach($titlescache as $key => $titleinfo)
					{
						if($post['postnum'] >= $key)
						{
							if(!$hascustomtitle)
							{
								$post['usertitle'] = $titleinfo['title'];
							}
							$post['stars']     = $titleinfo['stars'];
							$post['starimage'] = $titleinfo['starimage'];
							break;
						}
					}
				}

				if($usergroup['stars'])
				{
					$post['stars'] = $usergroup['stars'];
				}

				if(empty($post['starimage']))
				{
					$post['starimage'] = $usergroup['starimage'];
				}

				$postnum         = $post['postnum'];
				$post['postnum'] = my_number_format($post['postnum']);

				$post['userregdate'] = my_date($this->mybb->settings['regdateformat'], $post['regdate']);

				if($usergroup['usereputationsystem'] != 0 && $this->mybb->settings['enablereputation'] == 1 && ($this->mybb->settings['posrep'] || $this->mybb->settings['neurep'] || $this->mybb->settings['negrep']))
				{
					$post['userreputation'] = get_reputation($post['reputation'], $post['uid']);
				}

				if($this->mybb->settings['enablewarningsystem'] != 0 && $usergroup['canreceivewarnings'] != 0 && ($this->mybb->usergroup['canwarnusers'] != 0 || ($this->mybb->user['uid'] == $post['uid'] && $this->mybb->settings['canviewownwarning'] != 0)))
				{
					$warning_level = round($post['warningpoints'] / $this->mybb->settings['maxwarningpoints'] * 100);
					if($warning_level > 100)
					{
						$warning_level = 100;
					}
					$warning_level = get_colored_warning_level($warning_level);

					$post['warning_level'] = $warning_level;
				}
			}
			else
			{ // Message was posted by a guest or an unknown user
				$post['profilelink'] = format_name($post['username'], 1);

				if($usergroup['usertitle'])
				{
					$post['usertitle'] = $usergroup['usertitle'];
				}
				else
				{
					$post['usertitle'] = "Guest";
				}
			}
			$posts[$post['pid']] = $post;
			$post                = "";
		}

		if(count($posts) > $this->per_page)
		{
			$posts = array_slice($posts, $this->start, $this->per_page, true);
		}

		return $posts;
	}

	function check_forum_password($fid, $pid = 0)
	{
		global $forum_cache;

		$passed = false;

		if(!is_array($forum_cache))
		{
			$forum_cache = cache_forums();
			if(!$forum_cache)
			{
				return false;
			}
		}

		// Loop through each of parent forums to ensure we have a password for them too
		$parents = explode(',', $forum_cache[$fid]['parentlist']);
		rsort($parents);
		if(!empty($parents))
		{
			foreach($parents as $parent_id)
			{
				if($parent_id == $fid || $parent_id == $pid)
				{
					continue;
				}

				if($forum_cache[$parent_id]['password'] != "")
				{
					if(!$this->check_forum_password($parent_id, $fid))
					{
						return false;
					}
				}
			}
		}

		$password = $forum_cache[$fid]['password'];
		if($password)
		{
			if($this->data['forum_password'] == $password)
			{
				$passed = true;
			}
		}
		else
		{
			$passed = true;
		}

		return $passed;
	}
}