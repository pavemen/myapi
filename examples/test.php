<?php
require_once "config.php";

$url = 'users/1';

$result = curl($url, $code, array(), false, false);

echo 'Test of GET noAuth<pre>'.print_r($result, true).'</pre><br />';

$url = 'users/1';

$result = curl($url);

echo 'Test of GET with Auth<pre>'.print_r($result, true).'</pre><br />';

$attachment = array(
	'name'        => 'testing api',
	'link'        => 'http://www.google.com',
	'description' => 'working it'
);

$url = 'users/';

$result = curl($url, $code, $attachment);

echo 'Test of POST with Auth<pre>'.print_r($result, true).'</pre><br />';