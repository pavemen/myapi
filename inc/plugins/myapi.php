<?php
/**
 * MyBB plugin file to activate and configure API v1
 *
 * PHP version 5.3
 *
 * LICENSE: This software is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0
 * International License.
 *
 * @author     CommunityPlugins http://communityplugins.com
 * @copyright  2013-2014 Community Plugins
 * @license    http://creativecommons.org/licenses/by-nc-sa/4.0/
 * @version    1.0
 * @link       https://bitbucket.org/pavemen/myapi
 */
 
if(!defined("IN_MYBB"))
{
	die("Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.");
}

function myapi_info()
{
	return array(
		"name"          => "MyAPI",
		"description"   => "This Plugins performs a few edits on your board so the API can work",
		"website"       => "http://mybb.com",
		"author"        => "MyBB Group",
		"authorsite"    => "http://mybb.com",
		"version"       => "1.0",
		"guid"          => "",
		"compatibility" => "16*"
	);
}

function myapi_activate()
{
}

function myapi_deactivate()
{
}

function myapi_install()
{
	global $db;

	if(!$db->table_exists("myapi_sessions"))
	{
		$col = $db->build_create_table_collation();
		$db->query("CREATE TABLE `".TABLE_PREFIX."myapi_sessions` (
					`uid`		int(11)		NOT NULL,
					`token`		varchar(50)	NOT NULL,
					`ip`		varchar(50)	NOT NULL,
					`dateline`	bigint(30)	NOT NULL,
		PRIMARY KEY (`token`), KEY (`ip`)) ENGINE=MyISAM {$col}");
	}

	$query = $db->simple_select("settinggroups", "gid", "name='myapi'");
	$gid   = $db->fetch_field($query, "gid");

	if($gid)
	{
		$db->delete_query("settings", "gid=".$gid);
		$db->delete_query("settinggroups", "gid=".$gid);
	}

	$settings_group = array(
		"gid"         => "",
		"name"        => "myapi",
		"title"       => "MyAPI",
		"description" => "Settings for the MyAPI RESTful API.",
		"disporder"   => "50",
		"isdefault"   => "0"
	);

	$db->insert_query("settinggroups", $settings_group);
	$gid = $db->insert_id();

	$pluginsetting = array();

	$disporder = 0;

	$pluginsetting[] = array(
		"name"        => "myapi_enabled",
		"title"       => "Enable the API",
		"description" => "Master switch for the API.",
		"optionscode" => "onoff",
		"value"       => "0",
		"disporder"   => ++$disporder,
		"gid"         => $gid
	);

	$pluginsetting[] = array(
		"name"        => "myapi_key",
		"title"       => "Access token",
		"description" => "This token is used to verify if the request can be returned protected information or modify data. <strong>This is required</strong>. You can select a randomly generated string <a href=\"http://www.random.org/strings/?num=5&len=20&digits=on&upperalpha=on&loweralpha=on&unique=on&format=html&rnd=new\" target=\"_blank\">here</a>",
		"optionscode" => "text",
		"value"       => "",
		"disporder"   => ++$disporder,
		"gid"         => $gid
	);

	$pluginsetting[] = array(
		"name"        => "myapi_checkips",
		"title"       => "Limit IPs",
		"description" => "Limit IP addresses (IPv4) that are allowed to access the API",
		"optionscode" => "yesno",
		"value"       => "0",
		"disporder"   => ++$disporder,
		"gid"         => $gid
	);

	$pluginsetting[] = array(
		"name"        => "myapi_allowedips",
		"title"       => "List of IPs",
		"description" => "Comma separated list of IPs (IPv4) that are allowed to connect to the API. Only used if \'Limit IPs\' is enabled.",
		"optionscode" => "text",
		"value"       => "",
		"disporder"   => ++$disporder,
		"gid"         => $gid
	);

	$pluginsetting[] = array(
		"name"        => "myapi_checkauth",
		"title"       => "Use Basic Authentication",
		"description" => "Do you want to require an API specific username and password with every request?",
		"optionscode" => "yesno",
		"value"       => "0",
		"disporder"   => ++$disporder,
		"gid"         => $gid
	);

	$pluginsetting[] = array(
		"name"        => "myapi_authuser",
		"title"       => "API specific username.",
		"description" => "Only used if \'Use Basic Authentication\' is enabled.",
		"optionscode" => "text",
		"value"       => "",
		"disporder"   => ++$disporder,
		"gid"         => $gid
	);

	$pluginsetting[] = array(
		"name"        => "myapi_authpwd",
		"title"       => "API specific password.",
		"description" => "Only used if \'Use Basic Authentication\' is enabled.",
		"optionscode" => "text",
		"value"       => "",
		"disporder"   => ++$disporder,
		"gid"         => $gid
	);

	$pluginsetting[] = array(
		"name"        => "myapi_salt",
		"title"       => "Basic Authentication Salt",
		"description" => "Used to salt the password before being encrypted for transmission. Only used if \'Use Basic Authentication\' is enabled.  You can select a randomly generated string <a href=\"http://www.random.org/strings/?num=5&len=20&digits=on&upperalpha=on&loweralpha=on&unique=on&format=html&rnd=new\" target=\"_blank\">here</a>",
		"optionscode" => "text",
		"value"       => "",
		"disporder"   => ++$disporder,
		"gid"         => $gid
	);

	reset($pluginsetting);
	foreach($pluginsetting as $setting)
	{
		$db->insert_query("settings", $setting);
	}

	rebuild_settings();
}

function myapi_is_installed()
{
	global $db;

	return $db->table_exists("myapi_sessions");
}

function myapi_uninstall()
{
	global $db;
	if($db->table_exists("myapi_sessions"))
	{
		$db->drop_table("myapi_sessions");
	}

	$query = $db->simple_select("settinggroups", "gid", "name='myapi'");
	$gid   = $db->fetch_field($query, "gid");

	if($gid)
	{
		$db->delete_query("settings", "gid=".$gid);
		$db->delete_query("settinggroups", "gid=".$gid);
	}
}